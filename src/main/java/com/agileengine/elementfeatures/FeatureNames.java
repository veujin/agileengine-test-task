package com.agileengine.elementfeatures;

public class FeatureNames {
    public static final String PARENT = "parent";
    public static final String CHILDREN = "children";
    public static final String TEXT = "text";
    public static final String SELF_TAG = "self_tag";
}
