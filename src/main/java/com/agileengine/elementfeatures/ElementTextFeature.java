package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

public class ElementTextFeature extends ElementFeature {
    public ElementTextFeature(Element element, String text) {
        super(element, FeatureNames.TEXT, text);
    }
}
