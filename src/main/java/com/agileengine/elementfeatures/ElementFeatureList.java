package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

import java.util.List;
import java.util.Set;

public class ElementFeatureList {
    private Element element;
    private List<ElementFeature> features;

    public ElementFeatureList(Element element, List<ElementFeature> features) {
        this.element = element;
        this.features = features;
    }

    public List<ElementFeature> getFeatures() {
        return features;
    }

    public Element getElement() {
        return element;
    }
}
