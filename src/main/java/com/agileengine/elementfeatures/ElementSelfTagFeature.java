package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

public class ElementSelfTagFeature extends ElementFeature {
    public ElementSelfTagFeature(Element element, String tag) {
        super(element, FeatureNames.SELF_TAG, tag);
    }
}
