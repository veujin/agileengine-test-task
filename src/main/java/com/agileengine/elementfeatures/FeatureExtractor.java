package com.agileengine.elementfeatures;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FeatureExtractor {
    public static ElementFeatureList getElementFeaturesList(Element element) {
        List<ElementFeature> features = new ArrayList<>();
        features.addAll(getAttributesFeatures(element));
        features.addAll(getParentsFeatures(element));
        features.addAll(getChildesFeatures(element));
        features.add(getTextFeature(element));
        features.add(getSelfTagFeature(element));
        return new ElementFeatureList(element, features);
    }

    public static ElementSelfTagFeature getSelfTagFeature(Element element) {
        return new ElementSelfTagFeature(element, element.tagName());
    }

    public static ElementTextFeature getTextFeature(Element element) {
        return new ElementTextFeature(element, element.text());
    }

    public static List<ElementChildrenFeature> getChildesFeatures(Element element) {
        List<Element> elementChildes = element.children();
        return elementChildes
                .stream()
                .map(p -> new ElementChildrenFeature(element, getElementStringRepresentation(p)))
                .collect(Collectors.toList());
    }

    public static List<ElementParentFeature> getParentsFeatures(Element element) {
        List<Element> elementParents = element.parents();
        return elementParents
                .stream()
                .map(p -> new ElementParentFeature(element, getElementStringRepresentation(p)))
                .collect(Collectors.toList());
    }

    public static List<ElementAttributeFeatures> getAttributesFeatures(Element element) {
        List<Attribute> elementAttributes = element.attributes().asList();
        return elementAttributes
                .stream()
                .map(a -> new ElementAttributeFeatures(element, a.getKey(), a.getValue()))
                .collect(Collectors.toList());
    }

    private static String getElementStringRepresentation(Element element) {
        return String.format("%s - %s", element.tagName(), element.cssSelector());
    }
}
