package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

public class ElementChildrenFeature extends ElementFeature {

    public ElementChildrenFeature(Element element, String tag) {
        super(element, FeatureNames.CHILDREN, tag);
    }
}
