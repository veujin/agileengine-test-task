package com.agileengine.elementfeatures.matchscore;

import com.agileengine.elementfeatures.ElementFeature;
import com.agileengine.elementfeatures.ElementFeatureList;
import org.jsoup.nodes.Element;

import java.util.*;
import java.util.stream.Collectors;

public class FeatureListComparer {
    private final Map<ElementFeature, Double> featureFrequencyMap;

    public FeatureListComparer(List<ElementFeatureList> elementsFeatures)
    {
        this.featureFrequencyMap = buildFeatureFrequencyMap(elementsFeatures);
    }

    public Match getMatchScore(ElementFeatureList first, ElementFeatureList second, Element element)
    {
        List<ElementFeature> matchingFeatures = first
                .getFeatures()
                .stream()
                .filter(e -> second.getFeatures().contains(e))
                .collect(Collectors.toList());

        double score = matchingFeatures
                .stream()
                .distinct()
                .mapToDouble(featureFrequencyMap::get)
                .sum();

        return new Match(element, score, matchingFeatures);
    }

    private Map<ElementFeature, Double> buildFeatureFrequencyMap(List<ElementFeatureList> elementsFeatures)
    {
        Map<ElementFeature, Double> featureFrequencyMap = new HashMap<ElementFeature, Double>();
        elementsFeatures
                .stream()
                .flatMap(features -> features.getFeatures().stream())
                .forEach(feature ->
                {
                    if (!featureFrequencyMap.containsKey(feature))
                    {
                        featureFrequencyMap.put(feature, 1.);
                    }
                    else
                    {
                        featureFrequencyMap.put(feature, featureFrequencyMap.get(feature) + 1.);
                    }
                });

        featureFrequencyMap.forEach((feature, frequency) -> featureFrequencyMap.put(feature, 1/frequency));

        return featureFrequencyMap;
    }
}
