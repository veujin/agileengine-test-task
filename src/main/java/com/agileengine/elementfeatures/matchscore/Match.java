package com.agileengine.elementfeatures.matchscore;

import com.agileengine.elementfeatures.ElementFeature;
import com.agileengine.elementfeatures.ElementFeatureList;
import org.jsoup.nodes.Element;

import java.util.List;

public class Match {
    private Element element;
    private Double score;
    private List<ElementFeature> matchingFeatures;

    public Match(Element element, double score, List<ElementFeature> matchingFeatures) {
        this.element = element;
        this.score = score;
        this.matchingFeatures = matchingFeatures;
    }

    public Double getScore() {
        return score;
    }

    public Element getElement() {
        return element;
    }

    public List<ElementFeature> getMatchingFeatures() {
        return matchingFeatures;
    }
}
