package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

public class ElementParentFeature extends ElementFeature {
    public ElementParentFeature(Element element, String tag) {
        super(element, FeatureNames.PARENT,tag);
    }
}
