package com.agileengine.elementfeatures;

import org.jsoup.nodes.Element;

import java.util.Objects;

public class ElementFeature {
    private Element element;
    private final String key;
    private final String value;

    public ElementFeature(Element element, String key, String value) {
        this.element = element;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElementFeature that = (ElementFeature) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    @Override
    public String toString() {
        return String.format("[%s]: [%s]", key, value);
    }
}
