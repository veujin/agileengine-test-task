package com.agileengine;

import com.agileengine.elementfeatures.matchscore.Match;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Program {
    private static Logger logger = LoggerFactory.getLogger(Program.class);
    private static String CHARSET_NAME = "utf8";
    
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            logger.error("3 input attributes is required");
            System.exit(1);
        }

        Document originalDocument = Program.getDocument(args[0]);
        String targetElemetId = args[1];
        Document targetDocument = Program.getDocument(args[2]);

        List<Match> result = DomElementMatchFinder.findBestMatchesInDocument(
                originalDocument,
                targetElemetId,
                targetDocument);

        result.forEach(m -> logger.info(String.format(
                "Match with score %f\r\n" +
                        "\tCSS selector: %s\r\n" +
                        "\tMatching features: %s",
                m.getScore(),
                m.getElement().cssSelector(),
                String.join("\r\n\t\t", m.getMatchingFeatures().stream().map(x-> x.toString()).collect(Collectors.toList())))));
    }

    private static Document getDocument(String path) throws IOException {
        try {
            File file = new File(path);
            return Jsoup.parse(
                    file,
                    CHARSET_NAME,
                    file.getAbsolutePath());
        } catch (IOException e) {
            logger.error("Error reading {} file", path, e);
            throw e;
        }
    }
}
