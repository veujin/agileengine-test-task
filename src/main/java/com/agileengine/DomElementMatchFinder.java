package com.agileengine;

import com.agileengine.elementfeatures.*;
import com.agileengine.elementfeatures.matchscore.FeatureListComparer;
import com.agileengine.elementfeatures.matchscore.Match;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DomElementMatchFinder {

    public static List<Match> findBestMatchesInDocument(
            Document originalDocument,
            String targetElementId,
            Document targetDocument) {
        Element targetElement = originalDocument.getElementById(targetElementId);
        if (targetElement == null) {
            throw new IllegalArgumentException("Provided element id is not found in document");
        }

        // We collect features which describes element
        // for target element and for every element in target document
        ElementFeatureList targetElementFeatures = FeatureExtractor.getElementFeaturesList(targetElement);
        List<ElementFeatureList> targetDocumentElementsFeatures = getAllElementsFeaturesList(targetDocument);

        return findBestMatchByFeatures(targetDocumentElementsFeatures, targetElementFeatures);
    }

    private static List<ElementFeatureList> getAllElementsFeaturesList(Element element) {
        return element
            .getAllElements()
            .stream()
            .map(FeatureExtractor::getElementFeaturesList)
            .collect(Collectors.toList());
    }

    private static List<Match> findBestMatchByFeatures(
            List<ElementFeatureList> targetDocumentElementsFeatures,
            ElementFeatureList targetElementFeatures) {
        FeatureListComparer comparer = new FeatureListComparer(targetDocumentElementsFeatures);

        // Here we compare every element from target document with target element
        // and we get match scores. The more element have features in common, the higher the score.
        List<Match> matches = targetDocumentElementsFeatures
                .stream()
                .map(e -> comparer.getMatchScore(e, targetElementFeatures, e.getElement()))
                .collect(Collectors.toList());

        double bestScore = getBestScore(matches);

        return matches
                .stream()
                .filter(m -> m.getScore().compareTo(bestScore) == 0)
                .collect(Collectors.toList());
    }

    private static Double getBestScore(List<Match> matches) {
        return matches
                .stream()
                .max(Comparator.comparingDouble(Match::getScore))
                .get()
                .getScore();
    }
}
